<h1>8-bit Adder Logic Circuit</h1>
* On my first trial, I created an 8-bit adder that found the carry bit using 3 ANDs and 2 ORs rather than 2 ANDs and 1 OR

![Scratch Work](ScratchWork.jpeg)

![Original Adder](OriginalLogicGate.png)

* On the second attempt, although both worked, we simplified it down slightly. It did not work perfectly when thrown together with haste, but the general configuration worked.

![Improved Adder](ImprovedLogicGate.png)
