<h1>Altair Manual Notes, Part One</h1>

* Has 78 machine language instructions
* Users range from amateurs to experts in electronics and technology
* George Boole's detailed study showed how logic can be analyzed through arithmetic
* Boolean algebra works in true or false statements
* AND, OR, NOT
* NAND, NOR
* Exclusive (XOR)
* bits = binary digits
* Binary works in 0s and 1s (off and on)
* Five bit binary number would be 16s, 8s, 4s, 2s, 1s
* Octal on the ALTAIR 8800 takes 8 bits and splits them into 3 groups for each digit
* The highest octal number possible on the ALTAIR 8800 is 377
* 377 is represented as 11 111 111
* 2s, 1s. 4s, 2s, 1s. 4s, 2s, 1s.
