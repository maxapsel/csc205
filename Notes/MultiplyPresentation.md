# Multiplying Using the Altair!

![alt text](PresentationImages/Code.png)

## Breaking Down Important Sections

- ORG 0000, meaning that the origin of our program is 00.

- LXI  clears the H and L registers and initializes partial product

- RAL works like a shifter, shifting the entire byte to the left and setting the value of the carry bit to the zero bit position

| Before RAL | After RAL |
|-----------|----------|
| 1100 0110 | 1000 1101 |

- Note: partial product is a product obtained by multiplying a multiplicand by one digit of a multiplier having more than one digit.

- Iteration count. We set the value of register B to be the number of bits that the program is going to iterate thtough as it does partial product multiplication.

- In the Loop: we are doing repeated addition with each partial product to add up to the full product of multiplcation.

- Storage. We have space for up to 16 bits of output.

## Important Discoveries

- Using Simh, you can type help to pull up a guide walking you through all possible commands to use in the simulator.

- When checking inputs and outputs in Simh, they are in octal not hex.
