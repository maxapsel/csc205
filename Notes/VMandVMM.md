<h1>Virtual Memory</h1>

* __Virtual memory__ is a technique often used in a computer's operating system
* Uses hardware and software to make up for a lack of physical storage in a machine by moving the data from the RAM to disk storage temporarily
* RAM is much faster than __virtual memory__, so it would be faster to have a machine with 4 GB of physical RAM than to have 2 GB of physical RAM and 2 GB of __virtual memory__
* Data that is not currently being used gets transferred from physical RAM to __virtual memory__ to allow space for the programs and data being used at that moment
* A business owner uses their computer's __virtual memory__ system when running multiple applications simultaneously. The user tries to load their email in their browser window while also running word processing software, shift scheduling software and a content management system. The computer needs to run several programs at once, so it can use __virtual memory__ to open the user's email application while maintaining the operations of the other software programs.
* The __virtual memory manager__ processes the requests for memory made by the system and its applications
* The __virtual memory manager__ creates/manages address maps for processes and controls physical memory allocation
* The __virtual memory manager__ is essentially a translator between __virtual memory__ and physical memory when data must be shared between the two


![](PresentationImages/VirtualMemory.jpeg)
