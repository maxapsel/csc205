<h1>Encoder/Decoder Notes</h1>

* Encoders take an input of 2<sup>n</sup> lines and give an output of n bits
* Decoders take an input of n bits and output 2<sup>n</sup> lines
* The main type of encoder is called a priority encoder

<h2>Priority Encoder</h2>

* Creates a binary output address for the input of highest priority
* Priority is determined by the highest bit (furthest to the left)
* For example: an input 1010, the output would be a binary address for the 8ths digit bit, meaning 1111, 1110, 1011, 1001, 1100, 1101, and 1000 would have the same output.
* The output to any of the inputs listed above would be 11 to represent that the highest priority input is in the third slot.
| Input | Output |
| ----- | ------ |
| 0001 | 00 |
| 001x | 01 |
| 01xx | 10 |
| 1xxx | 11 |

<h3>Octal to Binary Encoder</h3>

* Has 8 (or 2<sup>3</sup>) input lines and 3 output bits
* Essentially acts the same as a priority encoder but with 8 inputs and 3 outputs
| Input | Output |
| ----- | ------ |
| 0000 0001 | 000 |
| 0000 001x | 001 |
| 0000 01xx | 010 |
| 0000 1xxx | 011 |
| 0001 xxxx | 100 |
| 001x xxxx | 101 |
| 01xx xxxx | 110 |
| 1xxx xxxx | 111 |


<h2>Encoder Logic Circuit</h2>

![Encoder](PresentationImages/encoder.jpeg)

<h2>Decoder Logic Circuit</h2>

![Decoder](PresentationImages/decoder.png)


* We can establish that the inputs we give these encoders have different meanings to us than simply binary digits
* If we want an encoder to represent compass directions, it may look something like this:

![Directions Diagram](PresentationImages/EncoderDecoderNotes.jpeg)
![Directions Table](PresentationImages/directions-table.png)

[Encoder Simulator](https://breadboard.electronics-course.com/4-2-encoder)

[Here](https://youtu.be/feBvhLFQEDk) is a video that helped Silas and I better understand encoders and decoders.
