<h1>Notes on Using an Assembler</h1>
* Even with the attempted fix from last class period (09/28) there is still an issue trying to get asm80 and dis80 to work
* There is not much presentable work to show for what I'll do for the rest of the day, but I intend on doing some online research on assemblers in general and what the source of the problem could be
* Today (09/30) we finally got the assembler to work and now I am trying to gain an understanding of how assembly language works
* If I can become familiar enough with assembly language to be able to make my own programs in it, just as I could in java/python/etc., I think coding the Altair would become much simpler for me
* Considering doing the Codecademy course for assembly language and see what I can get out of it
