<h1>Two's Complement</h1>
* Method of adding with negative numbers using binary on the Altair
* Whereas the user would often interpret 1000 0000 as 128 in decimal, it can now be interpreted as a -128
* This allows us to use any number within the range -128 to 127 rather than 0 to 128
* The reason this works is because the bits being carried ultimately gets carried into the unknown, where the user will not be able to see in in our 8-bit display
* This is notably different than the one's complement
<h2>One's Complement</h2>
* The one's complement achieves a similar goal of allowing us to interpret negative numbers 
* However, instead of 1000 0000 being -128, that first bit just acts to represent that the following number will be negative
* For example, 1000 0010 would be -2
* This gets tricky with addition and creates the concept of a negative zero
* 1000 0000 and 0000 0000 would have the same value, but one is negative and the other is positive
